from helloworld_mve import hello


def helloworldtest():
    assert hello.say() != "Goodbye World"
    assert hello.say() == "Hello World"
    assert hello.say() != "hello world"
